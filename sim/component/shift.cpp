#include <stdlib.h>
#include "Vtop_shift.h"
#include "verilated.h"
#include <cassert>
#include <iostream>

#define VL_DEBUG

int main(int argc, char **argv)
{
    // Initialize Verilators variables
	Verilated::commandArgs(argc, argv);
	// Create an instance of our module under test
	
    auto tb = new Vtop_shift;
    
    uint8_t a = 0b10001001;
    
    tb->A = a;
    //uint8_t a = 0b10001001;

    //left shift
    tb->dir = 0;
    tb->rot = 0;
    tb->eval();
    assert (tb->out == 0b00010010 && tb->status_c == 1 && tb->status_v == 1);

    //left rotate
    tb->rot = 1;
    tb->carry_in = 0;
    tb->eval();
    assert (tb->out == 0b00010010 && tb->status_c == 1 && tb->status_v == 1);

    //right rotate
    tb->dir = 1;
    tb->carry_in = 0;
    tb->eval();
    assert (tb->out == 0b01000100  && tb->status_c == 1 && tb->status_v == 0); //Status_v under review, come back to this.

    //right shift
    tb->rot = 0;
    tb->eval();
    assert (tb->out == 0b11000100  && tb->status_c == 1 && tb->status_v == 0); //Remember that shift right preserves left most bit since it is signed. 
    
    //use eval to change values.
    return 0;
}
