`include "adder.v"

// Declare a circuit that takes in two binary inputs and outputs the sum.
module top #(parameter WIDTH = 8) (
    input [WIDTH-1:0] A,
    input [WIDTH-1:0] B,
	input carry_in,
    output wire[WIDTH-1:0] out,
	output wire carry_out,
    output wire status_v
);
	adder #(.WIDTH(WIDTH)) adder(A, B, carry_in, out, carry_out, status_v);

endmodule 

