#include <stdlib.h>
#include "Vtop_NandNor.h"
#include "verilated.h"

int main(int argc, char **argv)
{
    // Initialize Verilators variables
	Verilated::commandArgs(argc, argv);
	// Create an instance of our module under test
	
    auto tb = new Vtop_NandNor;
    
    uint8_t a = 7;
    uint8_t b = 241;

    tb->A = a;
    tb->B = b;

    //Test AND
    tb->invert = 0;
    tb->sel = 0;
    tb->eval();
    assert (tb->out == (a & b));

    //Test NAND
    tb->invert = 1;
    tb->sel = 0;
    tb->eval();
    // For Negation we have an N-bit output, however the ~ operator upcasts to 32 bit.
    // We have to mask the output in the assertion to the N-bits, in this case 8-bit.
    assert(tb->out == (255 & (~(a & b))));

    //Test OR
    tb->invert = 0;
    tb->sel = 1;
    tb->eval();
    assert(tb->out == (a | b));

    //Test NOR
    tb->invert = 1;
    tb->sel = 1;
    tb->eval();
    //See comment for NAND case.
    assert(tb->out == (255 & (~(a | b))));
    
    //use eval to change values.
    return 0;
}
