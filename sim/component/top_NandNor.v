`include "NandNor.v"

// Input two values and choose between NAND, AND, NOR, OR.
module top #(parameter WIDTH = 8) (
    input [WIDTH-1:0] A,
    input [WIDTH-1:0] B,
    input wire invert,
    input wire sel,
    output wire [WIDTH-1:0] out,
    output wire status_v,
    output wire status_c
);
	NandNor #(.WIDTH(WIDTH)) NandNor(A, B, invert, sel, out, status_v, status_c);

endmodule 