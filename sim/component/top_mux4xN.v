`include "mux4xN.v"

// Take two inputs and select one to output.
module top #(parameter WIDTH = 8) (
    input wire [WIDTH-1:0] in1,
    input wire [WIDTH-1:0] in2,
    input wire [WIDTH-1:0] in3,
    input wire [WIDTH-1:0] in4,
    input wire [1:0] sel,
    output wire [WIDTH-1:0] out
);
	mux4xN #(.WIDTH(WIDTH)) mux(in1, in2, in3, in4, sel, out);

endmodule 