`include "shift.v"

// Input 8-bit value and shift or rotate the value.
module top #(parameter WIDTH = 8) (
    input wire [WIDTH-1:0] A,
    input wire dir,
    input wire rot,
    input wire carry_in,
    output reg [WIDTH-1:0] out,
    output wire status_v,
    output wire status_c
);
	shift #(.WIDTH(WIDTH)) shift(A, dir, rot, carry_in, out, status_v, status_c);

endmodule 