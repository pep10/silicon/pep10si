#include <stdlib.h>
#include "Vtop_demux2x4.h"
#include "verilated.h"
#include <cassert>

int main(int argc, char **argv)
{
    // Initialize Verilators variables
	Verilated::commandArgs(argc, argv);
	// Create an instance of our module under test
	
    auto tb = new Vtop_demux2x4;
    
    //Enable disabled
    tb->en = 0;
    tb->sel = 0b00;
    tb->eval();
    assert (tb->out == 0b0000);

    //Enable enabled, select first segment (sel == 00) -> (out == 0001)
    tb->en = 1;
    tb->eval();
    assert (tb->out == 0b0001);

    //Enable enabled, select second segment (sel == 01) -> (out == 0010)
    tb->sel = 0b01;
    tb->eval();
    assert (tb->out == 0b0010);

    //Enable enabled, select third segment (sel == 10) -> (out == 0100)
    tb->sel = 0b10;
    tb->eval();
    assert (tb->out == 0b0100);

    //Enable enabled, select last segment (sel == 11) -> (out == 1000)
    tb->sel = 0b11;
    tb->eval();
    assert (tb->out == 0b1000);
    
    //use eval to change values.
    return 0;
}
