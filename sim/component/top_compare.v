`include "compare.v"

// Input two values and choose between Greater than, or equal to.
module top #(parameter WIDTH = 8) (
    input wire[WIDTH-1:0] A,
    input wire[WIDTH-1:0] B,
    input wire eq,
    output reg out
);
	compare #(.WIDTH(WIDTH)) compare(A, B, eq, out);

endmodule 