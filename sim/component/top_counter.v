`include "counter.v"

// Declare a circuit that takes in two binary inputs and outputs the sum.
module top #(parameter WIDTH = 8) (
	input clock,
	input reset,
    output wire[WIDTH-1:0] out
);
	counter #(.WIDTH(WIDTH)) counter(clock, reset, out);

endmodule 

