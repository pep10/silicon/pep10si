#include <stdlib.h>
#include "Vtop_compare.h"
#include "verilated.h"
#include <cassert>

int main(int argc, char **argv)
{
    // Initialize Verilators variables
	Verilated::commandArgs(argc, argv);
	// Create an instance of our module under test
	
    auto tb = new Vtop_compare;
    
    uint8_t a = 241;
    uint8_t b = 7;

    tb->A = a;
    tb->B = b;

    //Test Greater Than
    tb->eq = 0;
    tb->eval();
    assert (tb->out == (a > b));

    //Test Less Than
    tb->A = b;
    tb->B = a;
    tb->eval();
    assert(tb->out == (a < b));

    //Test Equals(false result)
    tb->eq = 1;
    tb->eval();
    assert(tb->out == (a == b));

    //Test Equals(true result)
    tb->B = b;
    tb->eval();
    assert(tb->out == (b == b));
    
    //use eval to change values.
    return 0;
}
