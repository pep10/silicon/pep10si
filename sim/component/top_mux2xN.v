`include "mux2xN.v"

// Take two inputs and select one to output.
module top #(parameter WIDTH = 8) (
    input wire [WIDTH-1:0] in1,
    input wire [WIDTH-1:0] in2,
    input wire sel,
    output wire [WIDTH-1:0] out
);
	mux2xN #(.WIDTH(WIDTH)) mux(in1, in2, sel, out);

endmodule 

