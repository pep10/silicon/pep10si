#include <stdlib.h>
#include "Vtop_counter.h"
#include "verilated.h"


#include <vector>
#include <iostream>

int main(int argc, char **argv) {
	// Initialize Verilators variables
	Verilated::commandArgs(argc, argv);
	// Create an instance of our module under test
	auto tb = new Vtop_counter;
	static const int NUM_BITS = 8;
    uint64_t fails = 0;
    uint64_t runs = 0;
	// Activate the reset at all possible intervals.
	for (int reset_interval = 1; reset_interval<(1<<NUM_BITS); reset_interval++) {
		// Check all possible timesteps for our counter.
		for (int b=0; b<(1<<NUM_BITS); b++) {
			tb->clock = 0;
			tb->reset = 0;
			tb->eval();
			runs++;
			tb->clock = 1;
			tb->reset = b % reset_interval == 0; 
			tb->eval();
			if(tb->out > (1<<NUM_BITS)) {
				fails++;
			}
			else if(tb->out != ((b) % reset_interval) ) {
				fails++;
			}
		
		}
	}

    std::cout << "Ran " << runs << " unit tests." << std::endl;
    if(fails == 0) {
        std::cout << "Passed all unit tests." << std::endl;
    }
    else if(fails == 1) {
        std::cout << "Failed 1 unit test." << std::endl;
    }
    else{
        std::cout << "Failed " << fails <<" unit tests." << std::endl;
    }
    if(fails == 0) exit(EXIT_SUCCESS);
    else exit(EXIT_FAILURE);
}
