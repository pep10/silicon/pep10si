#include "Vtop_seg7decoder.h"
#include "verilated.h"
#include <iostream>
#include <stdint.h>
#include <iomanip>

void visual (uint8_t segIn);

int main(int argc, char **argv)
{
    // Initialize Verilators variables
	Verilated::commandArgs(argc, argv);
	// Create an instance of our module under test
	
    auto tb = new Vtop_seg7decoder;
    
    /*
    Tests change input value and call the visualizer function to show the characters.
    Testing 0-9 A-F
    */

    // Test 0
    tb->segment = 0;
    tb->eval();
    visual(tb->out);

    // Test 1
    tb->segment = 1;
    tb->eval();
    visual(tb->out);

    // Test 2
    tb->segment = 2;
    tb->eval();
    visual(tb->out);

    // Test 3
    tb->segment = 3;
    tb->eval();
    visual(tb->out);

    // Test 4
    tb->segment = 4;
    tb->eval();
    visual(tb->out);

    // Test 5
    tb->segment = 5;
    tb->eval();
    visual(tb->out);

    // Test 6
    tb->segment = 6;
    tb->eval();
    visual(tb->out);

    // Test 7
    tb->segment = 7;
    tb->eval();
    visual(tb->out);

    // Test 8
    tb->segment = 8;
    tb->eval();
    visual(tb->out);

    // Test 9
    tb->segment = 9;
    tb->eval();
    visual(tb->out);

    // Test A
    tb->segment = 10;
    tb->eval();
    visual(tb->out);

    // Test b
    tb->segment = 11;
    tb->eval();
    visual(tb->out);

    // Test C
    tb->segment = 12;
    tb->eval();
    visual(tb->out);

    // Test d
    tb->segment = 13;
    tb->eval();
    visual(tb->out);

    // Test E
    tb->segment = 14;
    tb->eval();
    visual(tb->out);

    // Test F
    tb->segment = 15;
    tb->eval();
    visual(tb->out);
    //END OF TESTS 0-9 A-F
    return 0;
}

void visual(uint8_t segIn)
{
    bool seg[7];
    //gfedcba
    for (int i=6; i >= 0; i--)
    {
        seg[6-i] = (segIn & (1 << i));
    }

    /*Read out the value stored to compare*/
    std::cout << "Binary value stored: ";
    for (int i=0; i < 7; i++)
    {
        std::cout << seg[i];
    }
    std::cout << std::endl;

    /*Display the LED for each value like it would be on the 7 segment display*/
    // A LED
    if(seg[6] == 0)
    {
        std::cout << "=====" << std::endl;
    }
    else
    {
        std::cout << std::endl;
    }

    // F AND B LEDS
    for (int k = 0; k < 3; k++)
    {
        // F LED
        if(seg[1] == 0)
        {
            std::cout << "|  ";
        }
        else
        {
            std::cout << "   ";
        }
        // B LED
        if(seg[5] == 0)
        {
            std::cout << " |" << std::endl;
        }
        else
        {
            std::cout << std::endl;
        }
    }

    // G LED
    if(seg[0] == 0)
    {
        std::cout << " === " << std::endl;
    }
    else
    {
        std::cout << std::endl;
    }

    // E AND C LEDS
    for (int k = 0; k < 3; k++)
    {
        // E LED
        if(seg[2] == 0)
        {
            std::cout << "|  ";
        }
        else
        {
            std::cout << "   ";
        }
        // C LED
        if(seg[4] == 0)
        {
            std::cout << " |" << std::endl;
        }
        else
        {
            std::cout << std::endl;
        }
    }
    // D LED
    if(seg[3] == 0)
    {
        std::cout << "=====" << std::endl;
    }
    else
    {
        std::cout << std::endl;
    }
    //separate the numbers
    std::cout << std::endl << "---------" << std::endl;
}