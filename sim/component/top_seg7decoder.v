`include "seg7decoder.v"

// Input the value for a 7 segment display and get the display code out
module top (
    input [3:0] segment,
    output wire[6:0] out
);
seg7decoder decoder(segment, out);

endmodule 