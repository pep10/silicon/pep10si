#include "Vtop_mux2xN.h"
#include "verilated.h"
#include <assert.h>

int main(int argc, char **argv)
{
    // Initialize Verilators variables
	Verilated::commandArgs(argc, argv);
	// Create an instance of our module under test
	auto tb = new Vtop_mux2xN;
    
    int a = 7;
    int b = 241;

    tb->in1 = a;
    tb->in2 = b;

    //Test 1 path in mux
    tb->sel = 1;
    tb->eval();
    assert(tb->out == b);

    //Test 0 path in mux
    tb->sel = 0;
    tb->eval();
    assert(tb->out == a);
    
    //use eval to change values.
    return 0;
}
