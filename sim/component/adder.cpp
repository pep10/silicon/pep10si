#include <stdlib.h>
#include "Vtop_adder.h"
#include "verilated.h"


#include <vector>
#include <iostream>

int main(int argc, char **argv) {
	// Initialize Verilators variables
	Verilated::commandArgs(argc, argv);
	// Create an instance of our module under test
	auto tb = new Vtop_adder;
    static const int NUM_BITS = 8;
    uint64_t fails = 0;
    uint64_t runs = 0;
    for(int carry_in=0; carry_in<2;carry_in++){
        for(int a=0; a<(1<<NUM_BITS);a++) {
            for (int b=0; b<(1<<NUM_BITS); b++) {
                tb->A = a;
                tb->B = b;
                tb->carry_in = carry_in;
                tb->eval();
                runs++;
                if (((~(a^b) & (a^tb->out))>>(NUM_BITS-1))&0x1 != tb->status_v)
                {
                    fails++;
                    std::cout << "Signed overflow incorrect." << std::endl;
                    std::cout << "Error occurred at A: " << a << " B: " << b << " Carry: " << carry_in << std::endl;
                }
                if(tb->out != (a+b+carry_in) % (1<<NUM_BITS)) {
                    fails++;
                    std::cout <<a<<" "<< b<< std::endl;
                    std::cout <<carry_in<<" "<< std::endl;
                    std::cout << (a+b+carry_in) % (1<<NUM_BITS) << " " << (int)tb->out << std::endl;
                    std::cout << ((int)(a+b+carry_in)&(1<<(NUM_BITS+1))) << " " << (int) tb->carry_out << std::endl;
                    goto end;
                }
                else if((bool)tb->carry_out != (bool)(a+b+carry_in)&(1<<(NUM_BITS+1))) {
                    fails++;
                }
            
            }
        }
    }
    end: int i = 0;
    std::cout << "Ran " << runs << " unit tests." << std::endl;
    if(fails == 0) {
        std::cout << "Passed all unit tests." << std::endl;
    }
    else if(fails == 1) {
        std::cout << "Failed 1 unit test." << std::endl;
    }
    else{
        std::cout << "Failed " << fails <<" unit tests." << std::endl;
    }
    if(fails == 0) exit(EXIT_SUCCESS);
    else exit(EXIT_FAILURE);
}
