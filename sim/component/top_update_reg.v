`include "counter.v"

// A counter circuit.
module top #(parameter WIDTH = 4) (
	input clock,
	input en_update,
	input en_primary,
    input [WIDTH-1:0] update,
	output difference,
	output [WIDTH-1:0] primary
);
	update_reg #(.WIDTH(WIDTH)) update_reg(.clock(clock),
	 .en_update(en_update), .en_primary(en_primary), .update(update), .difference(difference), .primary(primary));

endmodule 

