#include "Vtop_mux4xN.h"
#include "verilated.h"
#include <assert.h>

int main(int argc, char **argv)
{
    // Initialize Verilators variables
	Verilated::commandArgs(argc, argv);
	// Create an instance of our module under test
	auto tb = new Vtop_mux4xN;
    
    int a = 7;
    int b = 241;
    int c = 36;
    int d = 144;
    tb->in1 = a;
    tb->in2 = b;
    tb->in3 = c;
    tb->in4 = d;

    //Test 00 path in mux
    tb->sel = 0b00;
    tb->eval();
    assert(tb->out == a);

    //Test 01 path in mux
    tb->sel = 0b01;
    tb->eval();
    assert(tb->out == b);

    //Test 10 path in mux
    tb->sel = 0b10;
    tb->eval();
    assert(tb->out == c);

    //Test 11 path in mux
    tb->sel = 0b11;
    tb->eval();
    assert(tb->out == d);
    
    //use eval to change values.
    return 0;
}
