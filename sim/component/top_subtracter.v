`include "subtracter.v"

// Declare a circuit that takes in two binary inputs and outputs the difference.
module top #(parameter WIDTH = 8) (
    input [WIDTH-1:0] A,
    input [WIDTH-1:0] B,
	input carry_in,
	input invert_b,
    output wire[WIDTH-1:0] out,
	output wire carry_out,
    output wire status_v
);
	subtracter #(.WIDTH(WIDTH)) adder(A, B, carry_in, invert_b, out, carry_out, status_v);

endmodule 

