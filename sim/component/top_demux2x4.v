`include "demux2x4.v"

// Demux 2 by 4, used to select one of the 4 segements in the 7 segment display.
module top (
    input wire en,
    input wire [1:0] sel,
    output reg [3:0] out
);
	demux2x4 demux (en, sel, out);

endmodule 