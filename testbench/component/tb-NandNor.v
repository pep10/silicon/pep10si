`include "NandNor.v"
module testbench();

    specify
		specparam WIDTH = 8;
	endspecify

    reg [WIDTH-1:0] A;
    reg [WIDTH-1:0] B;
	reg sel;
    reg invert;
	wire [WIDTH-1:0] out;
    wire status_v;
    wire status_c;

    NandNor #(.WIDTH(WIDTH)) NandNor(A, B, invert, sel, out, status_v, status_c);

    //AND case
    initial begin
        A = 8'h4C;
        B = 8'h0A;
        sel = 0;
        invert = 0;

        $monitor("%g\t %b %b %h" , $time, sel, invert, out);
    end

    //NAND case
    initial begin
        #1;
        invert = 1;
    end

    //NOR case
    initial begin
        #1;
        sel = 1;
    end
    
    //OR case
    initial begin
        #2;
        invert = 0;
        $finish;
    end
endmodule