`include "shift.v"
module testbench();

    specify
		specparam WIDTH = 8;
	endspecify

    reg [WIDTH-1:0] A;
	reg dir;
    reg rot;
    wire carry_in;
	wire [WIDTH-1:0] out;
    wire status_v;
    wire status_c;

    shift #(.WIDTH(WIDTH)) shift(A, dir, rot, carry_in, out, status_v, status_c);

    //left shift
    initial begin
        A = 8'b10001001;
        dir = 0; //0 left, 1 right
        rot = 0; //0 shift, 1 rotate

        $monitor("%g\t %b %b %b %b %b" , $time, A, dir, rot, carry_in,out);
    end

    //left rotate
    always begin
        #1;
        rot = ~rot;
    end

    //right rotate
    always begin
        #2;
        dir = ~dir;

    end
    
    //right shift
    initial begin
        #6;
        rot = 0;
        $finish;
    end
endmodule