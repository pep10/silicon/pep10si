`include "counter.v"
module testbench();

	// Specify number of bits in counter.
	specify
		specparam WIDTH = 4;
	endspecify


	/* Declare all signals.*/
	reg clock;
	reg reset;
	wire [WIDTH-1:0] out;

	/* Instantiate modules.*/
	counter #(.WIDTH(WIDTH)) counter(clock, reset, out);


	/* Module behavior or unit tests.*/
	initial begin
		$dumpfile("counter.vcd");
		$dumpvars(0, testbench);
	end

	// Clock generator
	always #3 clock = ~clock;

	initial begin
		// Start device in reset state, with an upcoming rising edge.
		clock = 0;
		reset = 1;
		$display("Time \t Out");

		// Log changes to counter
		$monitor ("%g\t %b" , $time, out);
		// Lower reset after the first rising edge.
		#4;
		reset = 0;
	end

	// Toggle the reset every 400 ticks
	always #200 reset= ~reset;


	always begin
		// Good, loop bound is a function of module width.
        #600;
		//words
		$finish;
    end
endmodule