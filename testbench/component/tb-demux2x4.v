`include "demux2x4.v"
module testbench();

    reg en;
    reg [1:0] sel;
    wire [3:0] out;

    demux2x4 demux (en, sel, out);

    //Enable disabled, outputs 0000
    initial begin
        en = 0;
        sel = 2'b00;
        $monitor("%g\t %b %b %b" , $time, en, sel, out);
    end

    //Enable enabled, select first segment (sel == 00) -> (out == 0001)
    initial begin
        #1
        en = 1;
    end

    //Enable enabled, select second segment (sel == 01) -> (out == 0010)
    initial begin
        #1;
        sel = 2'b01;

    end

    //Enable enabled, select third segment (sel == 10) -> (out == 0100)
    initial begin
        #1;
        sel = 2'b10;

    end
    
    //Enable enabled, select last segment (sel == 11) -> (out == 1000)
    initial begin
        #2;
        sel = 2'b11;
        $finish;
    end
endmodule