`include "mux2xN.v"
module testbench();

    //Set the width to test an 8-bit multiplexer
    specify
        specparam WIDTH = 8;
    endspecify

    reg [WIDTH-1:0] in1;
    reg [WIDTH-1:0] in2;
    reg sel;
    wire [WIDTH-1:0] out;

    /* Module Instantiate */
    // Construct and pass in values
    mux2xN #(.WIDTH(WIDTH)) mux(in1, in2, sel, out);

    initial begin

        in1 = 8'h4C;
        in2 = 8'h0A;
        sel = 0;

        $monitor("%g\t %b %h" , $time, sel, out);
    end

    initial begin 
        #1;
        sel = 1;
    end

    initial begin 
        #2;
        sel = 0;
        $finish;
    end

endmodule