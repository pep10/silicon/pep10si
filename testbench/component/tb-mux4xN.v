`include "mux4xN.v"
module testbench();

    //Set the width to test an 8-bit multiplexer
    specify
        specparam WIDTH = 8;
    endspecify

    reg [WIDTH-1:0] in1;
    reg [WIDTH-1:0] in2;
    reg [WIDTH-1:0] in3;
    reg [WIDTH-1:0] in4;
    reg [1:0] sel;
    wire [WIDTH-1:0] out;

    /* Module Instantiate */
    // Construct and pass in values
    mux4xN #(.WIDTH(WIDTH)) mux(in1, in2, in3, in4, sel, out);

    //Test 00
    initial begin
        in1 = 8'b00110011;
        in2 = 8'b00000111;
	    in3 = 8'b00110000;
	    in3 = 8'b11000001;
        sel = 2'b00;

        $monitor("%g\t %b %h" , $time, sel, out);
    end

    //Test 01
    initial begin 
        #1;
        sel = 2'b01;
    end

    //Test 10
    initial begin 
        #1;
        sel = 2'b10;
    end

    //Test 11
    initial begin 
        #2;
        sel = 2'b11;
        $finish;
    end

endmodule