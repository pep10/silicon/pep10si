`include "compare.v"
module testbench();

    specify
		specparam WIDTH = 8;
	endspecify

    reg [WIDTH-1:0] A;
    reg [WIDTH-1:0] B;
	reg eq;
	wire out;

    compare #(.WIDTH(WIDTH)) compare(A, B, eq, out);

    //Greater than case
    initial begin
        A = 8'h4C;
        B = 8'h0A;
        eq = 0;

        $monitor("%g\t %b %b" , $time, eq, out);
    end

    //Less Than case
    initial begin
        #1
        A = 8'h0A;
        B = 8'h4C;
    end

    //Not equals case
    initial begin
        #1;
        eq = 0;

    end
    
    //equals case
    initial begin
        #2;
        A = 8'h3D;
        B = 8'h3D;
        $finish;
    end
endmodule