`include "update_reg.v"
module testbench();

	// Specify number of bits in counter.
	specify
		specparam WIDTH = 4;
	endspecify


	/* Declare all signals.*/
	reg clock;
	reg en_update, en_primary;
	reg [WIDTH-1:0] update;
	wire difference;
	wire [WIDTH-1:0] primary;

	/* Instantiate modules.*/
	update_reg #(.WIDTH(WIDTH)) mut(.clock(clock), .en_update(en_update), .en_primary(en_primary),
		.update(update), .difference(difference), .primary(primary));


	/* Module behavior or unit tests.*/
	initial begin
		$dumpfile("counter.vcd");
		$dumpvars(0, testbench);
	end

	// Clock generator
	always #2 clock = ~clock;

	initial begin
		clock = 0;
		$display("Time \t v_update v_old");
		// Log changes to counter
		$monitor ("%g\t %b   %b" , $time, mut.v_update, mut.v_primary);
	end

	initial begin
		#10;
		en_primary=0;
		en_update=1;
		update = 5;
		#5;
		en_primary=1;
	end



	always begin
        #100;
		$finish;
    end
endmodule