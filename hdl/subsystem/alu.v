`include "adder.v"

module alu
(
    input wire [7:0] A,
    input wire [7:0] B,
    input wire [3:0] select,
	input wire carry_in,
    output wire [7:0] out,
	output reg status_n,
	output reg status_z,
	output wire status_v,
	output wire status_c
);
	// Warning: Select is ignored.
	// The ADD w/C_IN operation is alwats selected.
	adder #(.WIDTH(8)) adder(A, B, carry_in, out, status_c, status_v);

	// Assign the status bits for add with carry in.
	always @(*) begin
		status_z = (out == 8'b0);
		status_n = out[7];
	end

endmodule