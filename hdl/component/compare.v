/*
Compare can do 2 options Greater than or Equal to. 
The eq input chooses to check difference or equality. 
*/

module compare #(parameter WIDTH = 1)
(
    input wire[WIDTH-1:0] A,
    input wire [WIDTH-1:0] B,
    input wire eq,
    output reg out
);

    always @(*) begin
        
        //Check if comparing if different or if equal.
        if (eq == 0)
            out = (A > B);
        else out = (A == B);
    end

endmodule