// Multiplexor 2 in, 1 out with selector.
module mux2xN #(parameter WIDTH = 1)
(
    input wire [WIDTH-1:0] in1,
    input wire [WIDTH-1:0] in2,
    input wire sel,
    output wire [WIDTH-1:0] out
);

    //Conditional  if(1) -> in2, if(0) -> in1
    assign out = sel ? in2 : in1;

endmodule