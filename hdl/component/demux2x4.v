// De-Multiplexor 2 in, 4 out with selector and enable.
module demux2x4
(
    input wire en,
    input wire [1:0] sel,
    output reg [3:0] out
);

    //select the correct segment to change
    always @(*) begin
      //if enabled set out to choose which digit to change
      if(en == 1) begin
        case(sel)
            2'b00 : out = 4'b0001; //set the segment to change
            2'b01 : out = 4'b0010;
            2'b10 : out = 4'b0100;
            2'b11 : out = 4'b1000;
        endcase;
      end
      //else nothing changes
      else out = 4'b0000;
   end

endmodule