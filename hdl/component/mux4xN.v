// 4 input Multiplexor 1 output

module mux4xN #(parameter WIDTH = 2) 
(
input wire [WIDTH-1:0] in1,
input wire [WIDTH-1:0] in2,
input wire [WIDTH-1:0] in3,
input wire [WIDTH-1:0] in4,
input wire [1:0] sel,
output reg [WIDTH-1:0] out
);

/*
Table for Case selection
SEL0  | SEL1  |  OUT
 0       0       in1
 0       1       in2
 1       0       in3
 1       1       in4
*/


   always @(*) begin
      //Input sel0 is high bit(2), sel1 is low bit(1)
      case(sel)
         2'b00 : out = in1;
         2'b01 : out = in2;
         2'b10 : out = in3;
         2'b11 : out = in4;
      endcase

   end

endmodule