// 7 Segment display number and letter decoder.
module seg7decoder
(
    input wire [3:0] segment,
    output wire [6:0] out
);
    //using a 16 long table of 7-bit values to decode segment code.
    reg [6:0] lookup [15:0];
    initial begin
        // Binary is in this order gfedcba
        lookup[0] =  7'b1000000; //0
        lookup[1] =  7'b1111001; //1
        lookup[2] =  7'b0100100; //2
        lookup[3] =  7'b0110000; //3
        lookup[4] =  7'b0011001; //4
        lookup[5] =  7'b0010010; //5
        lookup[6] =  7'b0000010; //6
        lookup[7] =  7'b1111000; //7
        lookup[8] =  7'b0000000; //8
        lookup[9] =  7'b0011000; //9
        lookup[10] =  7'b0001000; //A

        lookup[11] =  7'b0000011; //b
        lookup[12] =  7'b1000110; //C 
        lookup[13] =  7'b0100001; //d
        lookup[14] =  7'b0000110; //E
        lookup[15] =  7'b0001110; //F
    end
    
    assign out = lookup[segment];

endmodule