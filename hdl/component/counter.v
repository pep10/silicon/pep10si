// Module that increments its state by 1 every clock cycle.
// Has a synchronous reset.
module counter #(parameter WIDTH = 2) (
	input clock,
	input reset,
    output reg [WIDTH-1:0] out
);

	initial out = 0;

	always @(posedge clock) begin
		if(reset == 1) out <= 0;
		else out <= out + 1;
	end

endmodule