// Module that increments its state by 1 every clock cycle.
// Has a synchronous reset.
module update_reg #(parameter WIDTH = 2) (
	input clock,
	input en_update,
	input en_primary,
    input [WIDTH-1:0] update,
	output difference,
	output [WIDTH-1:0] primary
);

	reg [WIDTH-1:0] v_update = 0;
	reg [WIDTH-1:0] v_primary = 0;
	
	assign difference = (update == primary);
	assign primary = v_primary;

	always @(posedge clock) begin
		if(en_update == 1) begin
			v_update <= update; 
		end
		if(en_primary == 1) begin
			v_primary <= v_update;
		end
	end

endmodule