//This module will be used for the following boolean operators: NAND, AND, NOR, OR.

/* SELECTION PATTERN:

invert |   sel   | Operation
        0       0     AND
        0       1     OR
        1       0     NAND
        1       1     NOR

*/

module NandNor #(parameter WIDTH = 1)
(
    input [WIDTH-1:0] A,
    input [WIDTH-1:0] B,
    input wire invert,
    input wire sel,
    output reg [WIDTH-1:0] out,
    output reg status_v,
    output reg status_c
);
    
    //Using a nested selection we can select from the four operations.
    //For your sanity -> ((1,1) : (1,0)) : ((0,1) : (0,0))

    always @(*) begin
        if (sel == 0) begin
            if (invert == 0) out = (A & B);
            else out = ~(A & B);
            status_c = 0;
            status_v = 0;
        end
        else begin
            if (invert == 0) out = (A | B);
            else out = ~(A | B);
            status_c = 0;
            status_v = 0;
        end
    end

endmodule