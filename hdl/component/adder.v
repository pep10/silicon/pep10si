// Declare a circuit that takes in two binary inputs and outputs the sum.
module adder #(parameter WIDTH = 1) (
    input [WIDTH-1:0] A,
    input [WIDTH-1:0] B,
	input carry_in,
    output wire [WIDTH-1:0] out,
	output wire carry_out,
	output wire status_v
);

	// Perform addition with N+1 bits. Then, getting the carry-out only requires
	// fetching the most significant bit.
	wire [WIDTH:0] value;
	// Addition implemented using verilog operator! Easy to read and understand
	assign value = A + B + { {{(WIDTH){1'b0}} },carry_in};
	// Grab the low order N bits which compose the sum.
	assign out = value[WIDTH-1:0];
	// High order / most significant bit is the N+1'th bit, and is thus a carry out.
	assign carry_out = value[WIDTH];
	//Signed overflow: if the sign bit for the inputs are the same, but the output has a different sign bit, then your addition carried into the sign bit.
	assign status_v = ((A[WIDTH-1] == B[WIDTH-1]) & ~(A[WIDTH-1] == out[WIDTH-1]));

endmodule 

