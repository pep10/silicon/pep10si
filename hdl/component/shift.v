module shift #(parameter WIDTH = 1)
(
    input wire [WIDTH-1:0] A,
    input wire dir,
    input wire rot,
    input wire carry_in,
    output reg [WIDTH-1:0] out,
    output reg status_v,
    output reg status_c
);

    always @(*) begin
        //Using if else structure
        if (rot == 0 && dir == 0) begin//if rotate is 0 shift
            out = {A[WIDTH-2:0],1'b0}; //shift left
            status_c = A[WIDTH-1];
            status_v = ~(A[WIDTH-1] == A[WIDTH-2]);
        end
        else if (rot == 0 && dir == 1) begin
            out = {A[WIDTH-1], A[WIDTH-1:1]}; //shift right
            status_c = A[0];
            status_v = 0;
        end
        else if (rot == 1 && dir == 0) begin
            out = {A[WIDTH-2:0],carry_in}; //rotate left
            status_c = A[WIDTH-1]; //shift using the carry since carry is the extended first bit.
            status_v = ~(A[WIDTH-1] == A[WIDTH-2]);
        end
        else if (rot == 1 && dir == 1) begin
            out = {carry_in, A[WIDTH-1:1]}; //rotate right
            status_c = A[0]; //shift using carry 
            status_v = 0; //CURRENTLY UNDER REVIEW
        end
        else begin
            out = 8'b00000000; //error set to 0
        end
    end

endmodule